# from Reuters_Information_Retrieval import Index, MyParser, parsed_list
# import os

class PhraseQuery:

	def phrase_query(self, word1, word2, k):
		answer = []
		p1 = word1		#postings_list object
		p2 = word2		#postings_list object
		m = 0			#to iterate over postings_list
		n = 0			#to iterate over postings_list
		while m <= p1.length-1 and n <= p2.length-1:		#while both lists are not empty
			print('while both lists not empty', p1.postings_list[m].doc_id, p2.postings_list[n].doc_id)
			if p1.postings_list[m].doc_id == p2.postings_list[n].doc_id:		#if the doc ids match
				print('doc_ids equal')
				y = 0		#to iterate over positions
				z = 0		#to iterate over positions
				pp1 = p1.postings_list[m].positions		#assign positions of the first doc
				pp2 = p2.postings_list[n].positions		#assign positions of the second doc
				print('positions', pp1, pp2)
				while y < p1.postings_list[m].length and z < p2.postings_list[n].length:		#while both lists are not finished
						print('while y and z less than length', y, p1.postings_list[m].length, z, p2.postings_list[n].length)
						print('difference', pp2[z]-pp1[y])
						if pp2[z] - pp1[y] <= k and pp2[z] - pp1[y] > 0:		#if position 2 is withing one word from position 1
							if p1.postings_list[m].doc_id not in answer:
								answer.append(p1.postings_list[m].doc_id)		#append doc id to the answer list
							print('added')
							z += 1
							y += 1
						elif pp2[z] - pp1[y] > k:		#if position one is bigger
							y += 1						#move to next position in list 1
							print('y increased', y)		
						elif pp2[z] - pp1[y] < 0:		#if position two is bigger
							z += 1						#move to next position in list 2
							print('z increased', z)
							print()
				m += 1		#move to next doc id
				n += 1		#move to next doc id
				print('m and n increased', m, n)
			elif p2.postings_list[n].doc_id > p1.postings_list[m].doc_id:		#if doc id 2 is bigger than doc id 1
				m += 1		#move to next doc in list 1
				print('m increased', m)
			else:
				n +=1		#move to next doc in list 2
				print('n increased', n)
		return answer
		print(answer)







# path = '/Users/annahlyzova/Desktop/studies/TeamLab/example_reuters/'
# files_list = os.listdir(path)
# for file in files_list:
# 	if file.endswith(".sgm"):
# 		with open(path + file, "r") as f:
# 			lines = f.read()
# 			parser = MyParser(lines)
# index_obj = Index(parsed_list)
# print(index_obj.index['how'])
# print('')
# print(index_obj.index['much'])
# query_obj = PhraseQuery()
# query_obj.phrase_query(index_obj.index['how'], index_obj.index['much'])