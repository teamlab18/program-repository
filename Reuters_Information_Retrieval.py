import re, os, sys
import glob
from bs4 import BeautifulSoup
import time
from phraseQuery import PhraseQuery
start_time = time.time()


'''The parser outputs the parsed list of tuples of terms and doc ids'''
class MyParser():
	def __init__(self, lines):
		self.parse_text(lines)

	def parse_text(self, lines):
		soup = BeautifulSoup(lines,'html.parser')
		result_reuters = soup.find_all('reuters')	#find all similar texts with tags (reuters)
		for item in result_reuters:	# for each text with tags
			item = str(item)	#convert to string to parse further
			text_w_tags = BeautifulSoup(item, 'html.parser')
			doc_id = text_w_tags.reuters['newid']	#find doc id for a text
			pure_text = text_w_tags.get_text()	#get the pure text and normalize
			pure_text = re.sub(r'[\.+,!?:\n"\/]+', ' ', pure_text)
			pure_text = pure_text.lower().replace('\'', ' \'').split()
			i = 0
			for word in pure_text:	#for each word append it to the parsed list
				parsed_list.append((word, int(doc_id), i))
				i +=1
		parsed_list.sort()


class Posting:
	def __init__(self, element):
		self.doc_id = element[1]
		self.append_position(element)

	def append_position(self, element):
		self.length = 0
		self.positions = []
		self.positions.append(element[2])
		self.length = len(self.positions)

	def __repr__(self):
		return str(self.length) + "\t" + str(self.doc_id) + "\t" + str(self.positions)

'''Initially, when Postings List object is created, it has an attribute postings_list
which is empty. It has a function which appends an object of Posting to the list if the id is unique'''
class PostingsList:
	def __init__(self):
		self.length = 0
		self.postings_list = []
		
	def append_posting(self, element):
		self.postings_list.append(Posting(element))
		self.length = len(self.postings_list)

	def add_position(self, element):
		self.postings_list[-1].positions.append(element[2])
		self.postings_list[-1].length = len(self.postings_list[-1].positions)

	def __repr__(self):
		return str(self.length) + "\t" + str(self.postings_list)


class Dictionary:
	def __init__(self, element):
		self.term = element[0]
		self.postlist = PostingsList()

'''The main code is in here'''		
class Index:
	def __init__(self, elements):
		self.make_index(elements)

	def make_index(self, elements):
		self.index = {}
		prev_term = ''
		prev_posting = -456789
		for element in elements:
			if element == elements[0]:
				dict_obj = Dictionary(element)
				dict_obj.postlist.append_posting(element)
			elif element == elements[-1] and element[0] != prev_term:
				self.index[dict_obj.term] = dict_obj.postlist
				dict_obj = Dictionary(element)
				dict_obj.postlist.append_posting(element)
				self.index[dict_obj.term] = dict_obj.postlist
			elif element == elements[-1] and element[0] == prev_term:
				if element[1] == prev_posting:
					dict_obj.postlist.add_position(element)
				else:
					dict_obj.postlist.append_posting(element)
				self.index[dict_obj.term] = dict_obj.postlist
			elif element[0] != prev_term:
				self.index[dict_obj.term] = dict_obj.postlist
				dict_obj = Dictionary(element)
				dict_obj.postlist.append_posting(element)
			else:
				if element[1] == prev_posting:
					dict_obj.postlist.add_position(element)
				else:
					dict_obj.postlist.append_posting(element)


			prev_term = element[0]
			prev_posting = element[1]



'''This class does the query: it will stop only if you say 'exit'. It takes
a query input, normalizes and checks for each word if it's in index,
and then puts the PLs into setlists to do intersection'''
class Query:
	def query(self, index_dict):
		while True:
			setlists = []
			your_query = input('Your query:')
			queryTerm = your_query.lower().split(' ')
			if your_query == 'exit':
				print("--- %s seconds ---" % (time.time() - start_time))
				sys.exit()
			else:
				lists_to_compare = {}
				for term in queryTerm:
					if term in index_dict.keys():
						lists_to_compare[index_dict[term]] = queryTerm.index(term)
				print(len(lists_to_compare))
				for qTerm, index in lists_to_compare.items():
					if index == 0:
						p1 = qTerm
					else:
						p2 = qTerm
						k = index
						phraseQuery_obj = PhraseQuery()
						documents = phraseQuery_obj.phrase_query(p1, p2, k)
						setlists.append(set(documents))
			if setlists != [] and len(setlists) > 1:
				s = set.intersection(*setlists)
				if len(s)>=1:
					print('Documents found for the query \'',your_query,'\' are: ',s)
				else:
					print('Sorry. No intersection found.')
			elif len(setlists) == 1:
				print('Documents found for the query \'',your_query,'\' are: ', setlists[0])
			else:
				print('Sorry. No intersection found')



parsed_list = []

'''Open the file and parse it in a loop'''
'''Do the indexing and query'''
class Main():
	def __init__(self):
		self.run_index()

	def run_index(self):
		path = '/Users/annahlyzova/Desktop/studies/TeamLab/reuters/'
		files_list = os.listdir(path)
		for file in files_list:
			if file.endswith(".sgm"):
				with open(path + file, "r") as f:
					lines = f.read()
					parser = MyParser(lines)
		# print('parsed list', parsed_list)

		index_obj = Index(parsed_list)
		# print('')
		# print('index', index_obj.index)
		# print('for new', index_obj.index['new'])
		# print('for york', index_obj.index['york'])
		query_obj = Query().query(index_obj.index)
		# print(query_obj.query)


if __name__ == '__main__':
	main_obj = Main()